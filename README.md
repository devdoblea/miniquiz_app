Mini cuestionario que realiza 3 preguntas, evalúa si son falsas o 
verdaderas según se le haya indicado y entrega un resultado al final

Así quedó:

![Pagina principal](assets/Screenshot_2019-11-25-18-20-09.png);
![](assets/Screenshot_2019-11-25-18-20-30.png);
![](assets/Screenshot_2019-11-25-18-20-54.png);
![](assets/Screenshot_2019-11-25-18-21-03.png);

# miniquiz_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
